#include "fraction.h"

fraction::fraction() {

	num = 0;
	denom = 1;
}
fraction::fraction(int N) {

	num = N;
	denom = 1;
}
fraction::fraction(int N, int D) {

	num = N;
	denom = D;
}

void fraction::setNum(int n) {

	num = n;
}

void fraction::setDenom(int d) {

	denom = d;
}

int fraction::getNum() {


	return num;
}
int fraction::getDenom() {

	return denom;
}
fraction fraction::operator + (const fraction& f2) {//if + operater declare in source.cpp it use this function
	fraction temp(num * f2.denom + f2.num * denom, denom * f2.denom);
	return temp;
}
fraction fraction::operator - (const fraction& f2) {//if - operater declare in source.cpp it use this function
	fraction temp(num * f2.denom - f2.num * denom, denom * f2.denom);
	return temp;
}
fraction fraction::operator * (const fraction& f2) {//if * operater declare in source.cpp it use this function
	fraction temp(num * f2.num, denom * f2.denom);
	return temp;
}
fraction fraction::operator / (const fraction& f2) {//if / operater declare in source.cpp it use this function
	fraction temp(num * f2.denom, denom * f2.num);
	return temp;
}
fraction& fraction::operator ++ () { //use fraction to post increse numenator 
	num += denom;
	return *this;
}
fraction& fraction::operator ++ (int a) { //use fraction to pre increse numenator 
	num += denom;
	return *this;
}
fraction& fraction::operator -- () { //use fraction to post decrese numenator 
	num -= denom;
	return *this;
}
fraction& fraction::operator -- (int a) { //use fraction to pre decrese numenator
	num -= denom;
	return *this;
}
bool fraction::operator == (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom == denom * f2.num) { //if fraction1 equal fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator != (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom != denom * f2.num) { //if fraction1 not equal fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator < (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom < denom * f2.num) { //if fraction1 less than fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator <= (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom <= denom * f2.num) { //if fraction1 less than or equal fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator > (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom > denom * f2.num) { //if fraction1 more than fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
bool fraction::operator >= (const fraction& f2) { //use fraction1 and fraction2 and compare it
	if (num * f2.denom >= denom * f2.num) { //if fraction1 more than or equal fraction2 return true
		return true;
	}
	else
	{
		return false;
	}
}
