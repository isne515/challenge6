#ifndef FRACTION_H
#define FRACTION_H
class fraction
{
public:
	//constructors
	fraction();
	fraction(int N);
	fraction(int N, int D);
	//mutators
	void setNum(int); //set new numenator
	void setDenom(int); //set new denomenator
					   //accessors
	int getNum(); //keep numenator
	int getDenom(); //keep denomenator

	fraction operator + (const fraction& f2); //sum 2 fraction
	fraction operator - (const fraction& f2); //subtract 2 fraction
	fraction operator * (const fraction& f2); //multiply 2 fraction
	fraction operator / (const fraction& f2); //divide 2 fraction
	fraction& operator ++ (); //post increament for each fraction
	fraction& operator ++ (int a); // pre increament for each fraction
	fraction& operator -- (); //post decreament for each fraction
	fraction& operator -- (int a); //pre decreament for each fraction
	bool operator == (const fraction& f2); //compare 2 fraction when it equal 
	bool operator != (const fraction& f2); //compare 2 fraction when it not equal 
	bool operator < (const fraction& f2); //compare 2 fraction when fraction1 less than fraction2 
	bool operator <= (const fraction& f2); //compare 2 fraction when fraction1 less than and equal fraction2
	bool operator > (const fraction& f2);  //compare 2 fraction when fraction1 more than fraction2
	bool operator >= (const fraction& f2); //compare 2 fraction when fraction1 less than and equal fraction2
private:
	int num;
	int denom;
};
#endif // FRACTION_H#pragma once
