#include"fraction.h"
#include<iostream>

using namespace std;

int main() {

	fraction fr1, fr2, sum;

	int n1, d1, n2, d2;

	cout << "n1 is fraction1 numenator d1 is fraction1 dominator n2 is fraction2 numenator d2 is fraction2 dominator" << endl;

	cout << "Fill number(Ex:1 2 3 4 caution:don't fill dominator with 0):";
	cin >> n1 >> d1 >> n2 >> d2;

	fr1.setNum(n1);
	fr1.setDenom(d1);
	fr2.setNum(n2);
	fr2.setDenom(d2);

	cout << "Your fraction: ";
	cout << fr1.getNum() << "/" << fr1.getDenom() << "," << fr2.getNum() << "/" << fr2.getDenom() << endl << endl;
	sum = fr1 + fr2;
	cout << "fraction1+fraction2 = " << sum.getNum() << "/" << sum.getDenom() << endl << endl;
	sum = fr1 - fr2;
	cout << "fraction1-fraction2 = " << sum.getNum() << "/" << sum.getDenom() << endl << endl;
	sum = fr1 * fr2;
	cout << "fraction1*fraction2 = " << sum.getNum() << "/" << sum.getDenom() << endl << endl;
	sum = fr1 / fr2;
	cout << "fraction1/fraction2 = " << sum.getNum() << "/" << sum.getDenom() << endl << endl;

	fr1++; cout << "fraction1 post increament = " << fr1.getNum() << "/" << fr1.getDenom() << endl << endl;

	fr2++; cout << "fraction2 post increament = " << fr2.getNum() << "/" << fr2.getDenom() << endl << endl;

	++fr1; cout << "fraction1 pre increament = " << fr1.getNum() << "/" << fr1.getDenom() << endl << endl;

	++fr2; cout << "fraction2 pre increament = " << fr2.getNum() << "/" << fr2.getDenom() << endl << endl;

	fr1--; cout << "fraction1 post decreament = " << fr1.getNum() << "/" << fr1.getDenom() << endl << endl;

	fr2--; cout << "fraction2 post decreament = " << fr2.getNum() << "/" << fr2.getDenom() << endl << endl;

	--fr1; cout << "fraction1 pre decreament = " << fr1.getNum() << "/" << fr1.getDenom() << endl << endl;

	--fr2; cout << "fraction2 pre decreament = " << fr2.getNum() << "/" << fr2.getDenom() << endl << endl;

	if (fr1 >= fr2 || fr1 <= fr2) {
		if (fr1 > fr2) {
			cout << "Fraction1 more than fraction2" << endl << endl;
		}
		if (fr1 < fr2) {
			cout << "Fraction1 less than fraction2" << endl << endl;
		}
		if (fr1 == fr2) {
			cout << "Fraction1 equal fraction2" << endl << endl;
		}
		if (fr1 != fr2) {
			cout << "Fraction1 not equal fraction2" << endl << endl;
		}
	}
	system("PAUSE");
	return 0;
}
